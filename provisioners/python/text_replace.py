import shutil


def replace_in_file(file_name, old_text, new_text):
    shutil.copyfile(file_name, file_name + '.orig')
    reading_file = open(file_name, "r")

    new_file_content = ""

    for line in reading_file:
        stripped_line = line.strip()
        if stripped_line.startswith(old_text):
            new_line = stripped_line.replace(stripped_line, new_text)
        else:
            new_line = stripped_line
        new_file_content += new_line + "\n"

    reading_file.close()
    writing_file = open(file_name, "w")
    writing_file.write(new_file_content)
    writing_file.close()
