# **PETCLINIC DEPLOY (DEMO 1)**

* [Introduction](#introduction)
* [Requirements](#requirements)
* [Installation](#installation)
* [Starting the script](#starting-the-script)
* [Requirements of the Task](#requirements-of-the-task)


## INTRODUCTION
<p>
This repo contains the configuration and deployment scripts for the petclinic spring boot project. The process is fully 
automated and doesn't require any manual intervention. There is a file called `env_vars.yaml` which contains all the variables that script uses 
to configure the environment
</p>

## REQUIREMENTS
<p>This section contains information about the requirements and recommended packages to have in the host machine to configure environment and deploy the app.</p>

### Required to have:
> * Python 3
> * Vagrant
> * Ansible
> * Virtualbox
> * Bash shell

### Recommended to have:
> * Vagrant-faster plugin
> * Virtualbox Extension Pack

## INSTALLATION
<p>There is no exact installation procedure. You just need to clone the repo to the suitable folder.</p>

## Starting the script
<p>To start the deployment process you need to do the following steps:</p>

- Check the `env_vars.yaml` for right values of variables
- Execute the `python entrypoint.py`. The remaining process will be handled by the script

## Requirements of the Task

### Subtask I  - Infrastructure
- [x] Describe two virtual machines using Vagrantfile for deployment of the application (codename `APP_VM`) and the database (codename `DB_VM`).
- [x] Preferably use private networking feature for easy VM communication. `Used for inter-vm communication`
- [x] VMs should be either Centos or Ubuntu. `Ubuntu 20.04`
- [x] If not using private networking then `APP_VM` should have port 8080 forwarded to host. `Exposed 8080 for connection to APP from host`

### Subtask II - Database
- [x] Use any provisioning script that you created to install `MySQL` and any dependency on `DB_VM`. `Used Ansible`
- [x] Customize the mysql database to accept connections only from your vagrant private network subnet.
- [x] Create a non-root user and password (codename `DB_USER` and `DB_PASS`) in mysql. Use host environment variable to set these values and pass them to the Vagrantfile using `ENV`.
- [x] Create a database in mysql (codename `DB_NAME`) and grant all privileges for the `DB_USER` to access the database.

### Subtask III - Application
- [x] Create a non-root user (codename `APP_USER`) that will be used to run the application on `APP_VM`.
- [x] Use any provisioner to install `Java JDK`, git and any dependency on `APP_VM`. `Used Ansible`
- [x] Clone this repository to the working folder (codename `PROJECT_DIR`).
- [x] Run the application with the `APP_USER` using the `java -jar` command.

###Additional tasks
- [x] Check if the mysql server is up and running, and the database is accessible by created user (using either bash, python or ansible). If the database is not accessible - break the provisioning process. `Checked during the app provisioning via the Ansible`
- [x] Create a script to wait for up to 1 minute to check if the application is up and running. Use application healthcheck endpoint ~~http://localhost:8080/manage/health~~ (link is not working) `http://localhost:8080/actuator/health`  to see if response code is `200` and the status in the response body is up `({"status":"UP"})`. `Look at app_status_check.py`
- [ ] Implement database connection fallback logic. So that by default your java application should connect to mysql database on another VM. In case if that DB is not accessible application should connect to in-memory database (H2). :thinking:
- [ ] Instead Virtualbox use AWS provider. `Do not have AWS`
