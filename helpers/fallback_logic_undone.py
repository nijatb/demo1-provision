import os
import yaml


def find_var(var_name):
    with open(r'/vagrant/env_vars.yaml') as file:
        env_vars = yaml.safe_load(file)
        return env_vars[var_name]


app_user = find_var('APP_USER')
os.system('/vagrant/helpers/check_db_connection.sh')


db_status = os.popen('cat /home/' + app_user + '/db_connection.result')
db_status_output = db_status.read()

print(db_status_output)

