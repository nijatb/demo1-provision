#!/usr/bin/env bash
if mysql -u $db_user -p$db_pass -h $db_ip -D $db_name -e 'show tables;' > /dev/null; then
  echo 'yes' > /tmp/db_connection.result
else
  echo 'no' > /tmp/db_connection.result
fi