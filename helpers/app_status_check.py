import os
time = 0
working = 0
not_working = 0
while time <= 60:
    os.system('sleep 1')
    status_code = os.popen('curl -Is localhost:8080/actuator/health')
    status_code_output = status_code.read()
    status_message = os.popen('curl -s localhost:8080/actuator/health')
    status_message_output = status_message.read()

    if 'UP' in status_message_output:
        if '200' in status_code_output:
            if working == 0:
                os.system("echo 'The application is working' ")
                working = 1
                not_working = 0
    else:
        if not_working == 0:
            os.system("echo 'The application is not working' ")
            not_working = 1
            working = 0

    time += 1
