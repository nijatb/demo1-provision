# Entrypoint script is used to configure whole infrastructure before deploying it in order to have consistent env
# every time
from provisioners.python.text_replace import replace_in_file
import yaml
import os
import shutil


def find_var(var_name):
    with open(r'env_vars.yaml') as file:
        env_vars = yaml.safe_load(file)
        return env_vars[var_name]


vagrant_file_dir = './vagrant'
db_user = find_var('DB_USER')
db_ip = find_var('DB_HOST')
db_pass = find_var('DB_PASS')
db_name = find_var('DB_NAME')
db_host = find_var('DB_HOST')
db_port = find_var('DB_PORT')
db_root_pass = find_var('DB_PASS_ROOT')
db_vm = find_var('DB_VM')
app_vm = find_var('APP_VM')
app_dir = find_var('APP_DIR')
app_user = find_var('APP_USER')
app_ip = find_var('APP_VM_IP')
app_user_pass = find_var('APP_USER_PASS')


def change_db_prereqs():
    replace_in_file("helpers/db-prerequisites.sql", "CREATE USER",
                    "CREATE USER IF NOT EXISTS '" + db_user + "'@'" + app_ip + "' IDENTIFIED BY '" + db_pass + "';")
    replace_in_file("helpers/db-prerequisites.sql", "CREATE DATABASE", "CREATE DATABASE IF NOT EXISTS " + db_name + ";")
    replace_in_file("helpers/db-prerequisites.sql", "USE", "USE " + db_name + ";")
    replace_in_file("helpers/db-prerequisites.sql", "GRANT ALL PRIVILEGES",
                    "GRANT ALL PRIVILEGES ON " + db_name + ".* TO '" + db_user + "'@'" + app_ip + "';")


def change_db_pass():
    replace_in_file('helpers/script.exp', 'db_root_pass', 'db_root_pass="' + db_root_pass + '"')


def custom_replace_in_file(file_name, old_text, new_text):
    shutil.copyfile(file_name, file_name + '.orig')
    reading_file = open(file_name, "r")
    new_file_content = ""
    for line in reading_file:
        if old_text in line:
            word_start = line.index(" '")
            word_end = line.index("'", word_start+2)
            new_line = line.replace(line[word_start:word_end+1], " '" + new_text + "'")
        else:
            new_line = line
        new_file_content += new_line

    reading_file.close()
    writing_file = open(file_name, "w")
    writing_file.write(new_file_content)
    writing_file.close()


def change_app_provision():
    replaced_string = app_user_pass
    custom_replace_in_file('provisioners/ansible/app-provision.yml', 'password: "{{ ', replaced_string)


def change_service_file():
    replace_in_file('helpers/app_systemd.service', 'User=', 'User=' + app_user)
    replace_in_file('helpers/app_systemd.service', 'Environment="MYSQL_PASS=',
                    'Environment="MYSQL_PASS=' + db_pass + '"')
    replace_in_file('helpers/app_systemd.service', 'Environment="MYSQL_URL=',
                    'Environment="MYSQL_URL=jdbc:mysql://' + db_host + ':3306/' + db_name + '"')
    replace_in_file('helpers/app_systemd.service', 'Environment="MYSQL_USER=',
                    'Environment="MYSQL_USER=' + db_user + '"')
    replace_in_file('helpers/app_systemd.service', 'WorkingDirectory=', 'WorkingDirectory=' + app_dir + '/' + db_name)
    replace_in_file('helpers/app_systemd.service', 'ExecStart=',
                    'ExecStart=/usr/bin/java -Dspring.profiles.active=mysql -jar spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar')


def find_boxes_status():
    os.chdir(vagrant_file_dir)
    vagrant_status = os.popen('vagrant status')
    output = vagrant_status.read()
    start = output.index(':') + 1
    end = output.index(')')
    box_statuses = []
    while True:
        box_statuses.append(output[start:end + 1].strip())
        start = end + 2
        try:
            end = output.index(')', start)
            continue
        except ValueError:
            break
    return box_statuses


def vagrant_up(*args):
    # os.chdir(vagrant_file_dir)
    boxes = find_boxes_status()
    iteration = 0
    items = []
    for item in args:
        items.append(item)
    for info in boxes:
        box_name = info.split()[0]
        box_status = info.split()[1]
        if box_name in items:
            if box_status == 'running':
                pass
            elif box_status == 'saved':
                os.system('vagrant resume ' + box_name)
            elif box_status == 'poweroff':
                os.system('vagrant up ' + box_name)
            else:
                os.system('vagrant up ' + box_name)
        iteration += 1


change_app_provision()
change_db_pass()
change_db_prereqs()
change_service_file()
vagrant_up(app_vm, db_vm)
